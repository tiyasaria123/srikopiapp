class Drink{
  int id;
  String name;
  String image;
  int price;
  int discountPrice;
  double discount;
  bool isDiscount;
  bool isDiscountPrice;
  int counter = 1;
  bool isAdd = false;
  // int totalPrice = 0 ;


  Drink({
    this.name,
    this.image,
    this.id,
    this.discount,
    this.discountPrice,
    this.price,
    this.isDiscount,
    this.isDiscountPrice
  });

  int cartTotalPrice(){
    return price*counter;
  }
}

