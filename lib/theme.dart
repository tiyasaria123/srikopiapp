import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color orangeColor = Color(0xffFFD101);
Color blueColor = Color(0xff2F9CF1);

double edge = 16.0 ;

TextStyle blackSemibold = GoogleFonts.montserrat(
    fontWeight: FontWeight.w600,
    color: Color(0xff4E5356)
);

TextStyle blackNormal = GoogleFonts.montserrat(
    fontWeight: FontWeight.w400,
    color: Color(0xff4E5356)
);
TextStyle blackNormal2 = GoogleFonts.montserrat(
    fontWeight: FontWeight.w400,
    color: Colors.black
);

TextStyle blueMedium = GoogleFonts.montserrat(
    fontWeight: FontWeight.w500,
    color: blueColor
);

TextStyle blackMedium = GoogleFonts.montserrat(
    fontWeight: FontWeight.w500,
    color: Colors.black12
);

TextStyle blackBold = GoogleFonts.montserrat(
    fontWeight: FontWeight.w700,
    color: Colors.black
);

TextStyle orangeNormal = GoogleFonts.montserrat(
    fontWeight: FontWeight.w400,
    color: orangeColor
);

TextStyle greyNormal = GoogleFonts.montserrat(
    fontWeight: FontWeight.w400,
    color: Color(0xffa3a3a3)
);

TextStyle greyMenu= GoogleFonts.montserrat(
    fontWeight: FontWeight.w500,
    color: Color(0xff707585)
);

TextStyle diskon = GoogleFonts.montserrat(
    fontWeight: FontWeight.w600,
    color: Colors.black,
    fontSize: 10
);

TextStyle oldPriceText = GoogleFonts.montserrat(
    fontWeight: FontWeight.w400,
    color: Color(0xff252525),
    fontSize: 10
);

TextStyle pcstext = GoogleFonts.montserrat(
    fontWeight: FontWeight.w700,
    color: Color(0xffC5C7C9),
    fontSize: 10
);