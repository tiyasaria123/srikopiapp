import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:srikopi_app/theme.dart';

final List<String> imgList = ['asset/banner.png', 'asset/banner.png'];

final List<Widget> imageSliders = imgList
    .map((item) => Container(
  margin: EdgeInsets.only(right: 8),
      child: Image.asset(
  item,
  width: 362,
  height: 152,
),
    ))
    .toList();

class CarouselWithIndicatorDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;

  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                autoPlay: true,
                // enlargeCenterPage: true,
                autoPlayCurve: Curves.elasticOut,
                autoPlayInterval: Duration(seconds: 5),
                // aspectRatio: 3.0,
                // reverse: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
          DotsIndicator(
            dotsCount: 2,
            position: _current.toDouble(),
            decorator: DotsDecorator(
              size: const Size.square(8),
              color: orangeColor,
              activeSize: const Size(30, 8),
              activeColor: orangeColor,
              activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            ),
          ),
        ]);
  }
}
