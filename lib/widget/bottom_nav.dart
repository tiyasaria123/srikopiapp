import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:srikopi_app/theme.dart';

class BottomNavWidget extends StatelessWidget {
  const BottomNavWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0xff9e9e9e),
              blurRadius: 2,
              offset: Offset(0, 0), // changes position of shadow
            )
          ]
      ),
      child: Padding(
        padding: EdgeInsets.only( bottom: 8, left: 30, right: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.storefront_outlined, size: 18, color: orangeColor,),
                Text('Belanja', style: orangeNormal.copyWith(fontSize: 11),)
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.receipt_outlined, size: 18, color:  Color(0xffa3a3a3),),
                Text('Transaksi', style: greyNormal.copyWith(fontSize: 11),)
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.live_help_outlined, size: 18, color:  Color(0xffa3a3a3),),
                Text('Bantuan', style: greyNormal.copyWith(fontSize: 11),)
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.account_circle_outlined, size: 18, color:  Color(0xffa3a3a3),),
                Text('Profil', style: greyNormal.copyWith(fontSize: 11),)
              ],
            ),
          ],
        ),
      ),
    );
  }
}
