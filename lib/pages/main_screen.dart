import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:srikopi_app/model/drink.dart';



import 'package:srikopi_app/theme.dart';
import 'package:srikopi_app/widget/bottom_nav.dart';


import 'banner_screen.dart';



class MainScreen extends StatefulWidget {


  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<Drink> _drink = List<Drink>();
  List<Drink> _cartList = List<Drink>();

  @override
  void initState (){
    super.initState();
    _drinkItem();
  }



  @override
  Widget build(BuildContext context) {

    return SafeArea(
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          toolbarHeight: 150,
          flexibleSpace:  Container(
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 4,
                offset: Offset(0, 4), // changes position of shadow
              )
            ]),
            child: Padding(
              padding: EdgeInsets.all(edge),
              child: Column(
                children: [
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text('Kamu berbelanja di',
                              style: blackSemibold.copyWith(fontSize: 9)),
                          SizedBox(
                            height: 6,
                          ),
                          Row(
                            children: [
                              SvgPicture.asset('asset/store.svg'),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                'Kedai Baba',
                                style: blackBold.copyWith(fontSize: 11),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            'Ganti',
                            style: blueMedium.copyWith(fontSize: 10),
                          ),
                          SizedBox(width: 4),
                          SvgPicture.asset('asset/icon_back.svg')
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: edge),
                  //ini kolom search
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Color(0xffF1F2F6),
                        borderRadius: BorderRadius.circular(100)),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 8, bottom: 8, right: 16, left: 16),
                      child: Row(
                        children: [
                          Icon(Icons.search, color: Color(0xff707585)),
                          SizedBox(width: 6),
                          Text('Cari produk yang kamu cari...',
                              style: blackMedium.copyWith(fontSize: 10)),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              BannerScreen(),
              SizedBox(
                height: 6,
              ),
              //ini menu
              Container(
                color: Colors.white,
                height: 144,
                child: Padding(
                  padding: EdgeInsets.all(edge),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ButtonTheme(
                            minWidth: 64,
                            height: 64,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Color(0xffe8e8e8), width: 1),
                                borderRadius: BorderRadius.circular(10)),
                            child: RaisedButton(
                              onPressed: () {},
                              color: Colors.white,
                              child: Image.asset(
                                'asset/soda.png',
                                width: 32,
                                height: 32,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 6),
                          Text(
                            'Coffee Based\nBeverage',
                            style: greyMenu.copyWith(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(width: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ButtonTheme(
                            minWidth: 64,
                            height: 64,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Color(0xffe8e8e8), width: 1),
                                borderRadius: BorderRadius.circular(10)),
                            child: RaisedButton(
                              onPressed: () {},
                              color: Colors.white,
                              child: Image.asset(
                                'asset/cocktail.png',
                                width: 32,
                                height: 32,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 6),
                          Text(
                            'Non Coffre\nBeverage',
                            style: greyMenu.copyWith(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(width: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ButtonTheme(
                            minWidth: 64,
                            height: 64,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Color(0xffe8e8e8), width: 1),
                                borderRadius: BorderRadius.circular(10)),
                            child: RaisedButton(
                              onPressed: () {},
                              color: Colors.white,
                              child: Image.asset(
                                'asset/nuggets.png',
                                width: 32,
                                height: 32,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 6),
                          Text(
                            'Foody\nMoody',
                            style: greyMenu.copyWith(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(width: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ButtonTheme(
                            minWidth: 64,
                            height: 64,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Color(0xffe8e8e8), width: 1),
                                borderRadius: BorderRadius.circular(10)),
                            child: RaisedButton(
                              onPressed: () {},
                              color: Colors.white,
                              child: Image.asset(
                                'asset/cake.png',
                                width: 32,
                                height: 32,
                                alignment: Alignment.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 6),
                          Text(
                            'Subscription \n ',
                            style: greyMenu.copyWith(fontSize: 10),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 6),
              //ini itemnya
              Padding(
                padding: EdgeInsets.only(bottom: 16),
                child: Container(
                  color: Colors.white,
                  height: 400,
                  // width: MediaQuery.of(context).size.width ,
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: 16, right: 16, left: 16, bottom: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Produk Pilihan',
                                    style: blackBold.copyWith(fontSize: 13)),
                                Text('Produk pilihan yang terbaik untukmu',
                                    style: blackNormal.copyWith(fontSize: 10)),
                              ],
                            ),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Lihat Semua',
                                    style: blueMedium.copyWith(fontSize: 10),
                                  )
                                ]),
                          ],
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                            bottom: 8 , top: 8, right: 16, left: 16,
                          ),
                          child: Container(
                            height: 283,
                            // width: MediaQuery.of(context).size.width ,
                            child: _buildListView(),

                          )
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: (_cartList.length > 0)
          ? Container(
          decoration: BoxDecoration(
              color: orangeColor, borderRadius: BorderRadius.circular(5)),
          width: 320,
          height: 54,
          child: Padding(
            padding: EdgeInsets.only(
              top: 8,
              bottom: 8,
              left: edge,
              right: edge,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset('asset/cart.svg'),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text( _cartList.length.toString() + ' Item', style: blackBold.copyWith(fontSize: 14)),
                              Text('Kedai Baba',
                                  style: blackNormal2.copyWith(fontSize: 12))
                            ],
                          ),
                  ],
                ),
                Text(
                  NumberFormat.currency(
                    locale : 'id',
                      symbol : 'Rp ',
                      decimalDigits : 0
                  ).format(cartTotalPrice()),
                  style: blackBold.copyWith(fontSize: 14),
                )
              ],
            ),

          ),
        )
        :  Container(),
        floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
        bottomNavigationBar: BottomNavWidget(),
      ),
    );
  }

  ListView _buildListView(){
    return ListView.builder(
      scrollDirection: Axis.horizontal,
        itemCount: _drink.length,
        itemBuilder: (context, index){
        var item = _drink[index];
        return Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7)
          ),
          elevation: 1,
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(7),
                  topRight: Radius.circular(7),
                ),
                child: Stack(children: [
                  Image.asset(
                    item.image,
                    width: 150,
                    height: 129,
                    fit: BoxFit.cover,
                  ),
                  item.isDiscount
                      ? Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(5),
                              topLeft: Radius.circular(5)),
                          color: orangeColor),
                      width: 75,
                      height: 30,
                      child: Center(
                        child: Text(
                          "Disk. ${item.discount}%",
                          style: diskon,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  )
                      : Container()
                ]),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8),
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.name,
                          style: diskon.copyWith(fontSize: 14),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        item.isDiscountPrice
                            ? Container(
                          child: Text(
                              NumberFormat.currency(
                                  locale : 'id',
                                  symbol : 'Rp ',
                                  decimalDigits : 0
                              ).format(item.discountPrice),
                              style: oldPriceText.copyWith(
                                  decoration: TextDecoration.lineThrough)),
                        )
                            : Container(
                          child: SizedBox(
                            height: 10,
                          ),
                        ),
                        SizedBox(height: 4),
                        Row(
                          children: [
                            Text(
                              NumberFormat.currency(
                                  locale : 'id',
                                  symbol : 'Rp ',
                                  decimalDigits : 0
                              ).format(item.price),
                              style: blackBold.copyWith(fontSize: 13),
                            ),
                            Text(
                              '/pcs',
                              style: pcstext,
                            )
                          ],
                        ),
                        SizedBox(height: 16),

                      ],
                    ),
                     Container(
                        child: (item.isAdd == false)
                            ? ElevatedButton(
                          onPressed: () {
                              setState(() {
                                if (!_cartList.contains(item))
                                  _cartList.add(item);
                                // else
                                //   _cartList.remove(item);
                                item.isAdd = true;
                                // ignore: unnecessary_statements
                                item.counter ;
                               // ignore: unnecessary_statements


                              });
                          },
                          child: Text('Tambahkan',
                                style: blackBold.copyWith(fontSize: 10)),
                          style: ElevatedButton.styleFrom(primary: orangeColor),
                        )
                            : Row(
                          children: [
                            SizedBox(
                              height: 30,
                              width: 30,
                              child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      if(item.counter == 1 ){
                                        _cartList.remove(item);
                                        item.isAdd = false;
                                        // ignore: unnecessary_statements
                                      }else{
                                        item.counter -= 1;
                                      }

                                      // ignore: unnecessary_statement
                                    });
                                  },
                                  child: Center(
                                      child: Text(
                                        '-',
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.black),
                                        textAlign: TextAlign.center,
                                      )),
                                  style: ElevatedButton.styleFrom(
                                      side: BorderSide(
                                          width: 1, color: Color(0xffc0c0c0)),
                                      primary: Colors.white),
                                ),
                            ),
                            Container(
                              color: Colors.white,
                              height: 30,
                              width: 58,
                              child: Center(
                               child: Text(
                                 item.counter.toString(),
                               )
                              )
                            ),
                            SizedBox(
                              height: 30,
                              width: 30,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Color(0xffFFD101),
                                ),
                                onPressed: () {
                                  setState(() {
                                    item.counter += 1;
                                  });
                                },
                                child: Center(
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                      textAlign: TextAlign.center,
                                    )),
                              ),
                            ),


                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        );
        },
    );
  }

  void _drinkItem() {
    var list = <Drink>[
      Drink(
        id: 1,
        name: 'Kopi Petir',
        image: 'asset/ice1.png',
        price : 10000 ,
        discountPrice: 15000 ,
        discount: 53,
        isDiscount: true,
        isDiscountPrice: true

      ),
      Drink(
        id: 2,
        name: 'Kopi Petir',
        image: 'asset/ice2.png',
        price: 8000 ,
        discountPrice: 15000 ,
        discount: 53,
        isDiscount: false,
        isDiscountPrice : false,
      ),
      Drink(
        id: 3,
        name: 'Kopi Petir',
        image: 'asset/ice1.png',
        price : 5000 ,
        discountPrice: 15000 ,
        discount: 53,
        isDiscount: false,
        isDiscountPrice: false,
      ),
      Drink(
        id: 4,
        name: 'Kopi Petir',
        image: 'asset/ice1.png',
        price: 9000 ,
        discountPrice: 15000 ,
        discount: 53,
        isDiscount: true,
        isDiscountPrice: false,
      ),
    ];

    setState((){
      _drink = list;
    });
  }

  int cartTotalPrice(){
    int total = 0;
    _cartList.forEach((item) {
      total += item.cartTotalPrice();
    });
    return total ;
  }


}








