import 'package:flutter/material.dart';
import 'package:srikopi_app/widget/banner_widget.dart';
import 'package:srikopi_app/theme.dart';

class BannerScreen extends StatelessWidget {
  const BannerScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(edge),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Informasi Terbaru', style: blackBold.copyWith(fontSize: 13)),
            Text('Informasi Terbaru hanya untukmu', style: blackNormal.copyWith(fontSize: 10)),
            CarouselWithIndicatorDemo()
          ],
        ),
      ),
    );
  }
}
